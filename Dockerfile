#syntax=docker/dockerfile:1.4
# base os target
FROM php:7.4.33-cli-alpine3.16 as os
WORKDIR /app
RUN apk --no-cache add  \
    unzip  \
    && rm -rf /var/cache/apk/*
COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/
RUN set -eux; \
    install-php-extensions \
    zip \
    bcmath \
    && rm -rf /tmp/*
ENV COMPOSER_ALLOW_SUPERUSER=1
COPY --from=composer/composer:2-bin /composer /usr/bin/composer

## intermediate layers for vendor installation (allows to exclude cache files from final target)
FROM os as vendor-production
WORKDIR /build
COPY --link composer.*  ./
RUN set -eux; \
    composer install --no-cache --prefer-dist --no-dev --no-autoloader --no-scripts --no-progress

FROM os as vendor-development
WORKDIR /build
COPY --link composer.* ./
RUN set -eux; \
    composer install --no-cache --prefer-dist --no-autoloader --no-scripts --no-progress

## Runtime targets
# Local target for development with /app volume mount
FROM os as runtime-local
ENV COMPOSER_HOME=/.composer
RUN apk --no-cache add  \
    htop \
    procps \
    && rm -rf /var/cache/apk/*
RUN set -eux;  \
    install-php-extensions \
    xdebug-3.1.2
RUN cp $PHP_INI_DIR/php.ini-development $PHP_INI_DIR/php.ini
COPY --link .docker/development/php/php-override.ini $PHP_INI_DIR/conf.d/php-override.ini
ARG USER=www-data
ARG UID=1000
ARG GID=1000
RUN addgroup -g ${GID} ifx \
    && adduser -D -u ${UID} -G ifx local
USER ${USER}
WORKDIR /app

# QA Target for pipeline jobs with development vendors
FROM runtime-local as runtime-development
COPY --chown=www-data:www-data --from=vendor-development /build/vendor /app/vendor
COPY --chown=www-data:www-data --link . .
RUN set -eux; \
    composer dump-autoload