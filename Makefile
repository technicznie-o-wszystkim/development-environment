.DEFAULT_GOAL := help

help:  ## Display this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

##@ Application commands
create: copy-env build install start ## Create application from scratch (one-time only command)
start: ## Start application (application must be created before)
	docker compose up -d
stop: ## Stop application (will preserve state of the application)
	docker compose stop
restart: start ## Restart application (docker-compose configuration will be reloaded)
refresh: destroy create ## Refresh infrastructure configuration of a environment and application
rebuild: build restart

install: ## Prepare application environment
	docker compose run --rm php composer install --prefer-dist

update: ## Update composer dependency
	docker compose run --rm php composer update --prefer-dist

destroy: down ## Stop application and remove all volumes and networks

build: ## Build local images
	docker compose pull
	docker compose build

copy-env: ## Copying local override config files
	cp -f compose.override-dist.yaml compose.override.yaml

down: ## stop and remove containers and volumes
	docker compose down -v


##@ Container commands
shell: ## Login to main container with shell
	docker compose run --rm -it php sh

##@ QA commands
qa: test phpstan cs ## run all QA tools

test: ## Run phpunit
	docker compose run --rm php composer qa:phpunit

phpstan: ## Run Phpstan
	docker compose run --rm php composer qa:phpstan

cs: ## Dry-Run ECS
	docker compose run --rm php composer qa:cs:check

cs-fix: ## Fix ECS issues
	docker compose run --rm php composer qa:cs:fix

ch-reinstall:
	docker compose run --rm php vendor/bin/captainhook uninstall -q
	docker compose run --rm php vendor/bin/captainhook install --only-enabled -s -f