# development-environment



## Docker & Docker Compose



## Makefile
* https://stackoverflow.com/questions/2532234/how-to-run-a-makefile-in-windows

## Composer
* https://getcomposer.org/
* https://packagist.org/
  * https://packagist.com/
* https://semver.org/

## QA
* https://github.com/sebastianbergmann/phpunit
* https://github.com/PHPCSStandards/PHP_CodeSniffer/
* https://github.com/PHP-CS-Fixer/PHP-CS-Fixer
* https://github.com/easy-coding-standard/easy-coding-standard
  * https://tomasvotruba.com/blog/2017/05/03/combine-power-of-php-code-sniffer-and-php-cs-fixer-in-3-lines/

## Git Hooks
* https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks
* https://github.com/captainhookphp/captainhook
* https://captainhookphp.github.io/captainhook/index.html

## PSR / PER
* https://www.php-fig.org/psr/psr-1/ (basic coding)
* https://www.php-fig.org/psr/psr-4/ (auto-loading)
* https://www.php-fig.org/psr/psr-12/ (coding standard - deprecated but still popular)
* https://www.php-fig.org/per/coding-style/ (coding standard - current)

## Gitlab CI
* https://docs.gitlab.com/ee/ci/yaml/

### MAKEFILE solution for windows:

The error message you are encountering typically happens when using the awk command on a Windows machine because the Windows shell may not interpret the command correctly.

Solution
If you're on Windows and using awk through Git Bash or another Unix-like shell, you should make sure that awk is available and your Makefile command is properly quoted for the environment.

Updated Makefile for Windows Users
Try using this Makefile command instead:


```
help: ## Display this help
@awk "BEGIN {FS = \":.*##\"; printf \"\nUsage:\n  make \033[36m<target>\033[0m\n\"} /^[a-zA-Z_-]+:.*?##/ { printf \"  \033[36m%-15s\033[0m %s\n\", \$$1, \$$2 } /^##@/ { printf \"\n\033[1m%s\033[0m\n\", substr(\$$0, 5) }" $(MAKEFILE_LIST)
```

Explanation of Changes

```
Double Quotes: Use double quotes (") instead of single quotes ('), so the command can work correctly in Windows shells.
Escaping Dollar Signs: Double $ signs (\$$1 instead of $1) are required to avoid interpretation by make.
Unix-like Shells in Windows
Ensure that you're running make through a compatible shell like Git Bash, WSL (Windows Subsystem for Linux), or Cygwin. This will provide a Unix-like environment that should work well with Makefiles.
```

## PHP Polish Conferences
* https://summit.phpers.pl/pl/
* https://phpcon.pl (there is no date set for 2024 yet - it should be announced around June)
* https://phpers.pl/ (local meetups)
* https://4developers.org.pl/ (interdisciplinary conference)