<?php

declare(strict_types=1);

use PhpCsFixer\Fixer\PhpUnit\PhpUnitMethodCasingFixer;
use Symplify\EasyCodingStandard\Config\ECSConfig;
use Symplify\EasyCodingStandard\ValueObject\Set\SetList;

return static function (ECSConfig $config): void {
    $config->parallel();
    $config->import(SetList::PSR_12);
    $config->import(SetList::CLEAN_CODE);
    $config->import(SetList::STRICT);
    $config->import(SetList::COMMON);
    $config->import(SetList::CONTROL_STRUCTURES);
    $config->import(SetList::NAMESPACES);
    $config->import(SetList::PHPUNIT);
    $config->skip([PhpUnitMethodCasingFixer::class]);
    $config->paths([
        __DIR__ . '/src',
        __DIR__ . '/tests',
    ]);

    $config->fileExtensions(['php']);
    $config->cacheDirectory('var/.ecs_cache');
};
