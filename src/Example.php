<?php

declare(strict_types=1);

namespace Tow\App;

class Example
{
    public function run(): void
    {
        phpinfo();
    }

    /**
     * Custom Comment
     */
    public function onlyForTest(int $input, float $weight): ?int
    {
        if ($weight < 1.33) {
            return null;
        }
        $calculatedValue = $input * $weight;
        return (int) floor($calculatedValue);
    }
}
