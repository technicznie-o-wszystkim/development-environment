<?php

declare(strict_types=1);

namespace Tow\App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use Tow\App\Example;

class ExampleTest extends TestCase
{
    public function test_should_generate_null_when_weight_to_low(): void
    {
        // given
        $value = 3;
        $weight = 1.32;

        // when
        $sut = new Example();
        $result = $sut->onlyForTest($value, $weight);

        // then
        $this->assertEquals(null, $result);
    }

    public function test_should_generate_integer(): void
    {
        // given
        $value = 3;
        $weight = 3.45;

        // when
        $sut = new Example();
        $result = $sut->onlyForTest($value, $weight);

        // then
        $this->assertEquals(10, $result);
    }
}
